namespace RegieMag.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IdAsString : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Products", new[] { "Owner_Id" });
            DropColumn("dbo.Products", "OwnerID");
            RenameColumn(table: "dbo.Products", name: "Owner_Id", newName: "OwnerID");
            AlterColumn("dbo.Products", "OwnerID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Products", "OwnerID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "OwnerID" });
            AlterColumn("dbo.Products", "OwnerID", c => c.Int());
            RenameColumn(table: "dbo.Products", name: "OwnerID", newName: "Owner_Id");
            AddColumn("dbo.Products", "OwnerID", c => c.Int());
            CreateIndex("dbo.Products", "Owner_Id");
        }
    }
}
