﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RegieMag.ViewModels
{
    public class ProductViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Produsul trebuie sa aiba nume")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Numele produsului")]
        [RegularExpression(@"^[a-zA-Z0-9'-'\s]*$", ErrorMessage = "Numele trebuie sa contina litere si cifre")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Produsul trebuie sa aiba descriere de minim 10 caractere")]
        [StringLength(200, MinimumLength = 10, ErrorMessage = "Descrierea produsului")]
        [RegularExpression(@"^[,;a-zA-Z0-9'-'\s]*$", ErrorMessage = "Descrierea trebuie sa contina litere si cifre")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Required(ErrorMessage = "Pretul este obligatoriu")]
        [Range(0.00, 10000, ErrorMessage = "Pretul trebuie sa fie intre 0.00 lei si 10000.00 lei")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:c}")]
        [RegularExpression("[0-9]+(\\.[0-9][0-9]?)?", ErrorMessage = "Pretul trebuie scris cu precizie de 2 zecimale")]
        public decimal Price { get; set; }
        [Display(Name = "Categorie")]
        public int CategoryID { get; set; }
        public SelectList CategoryList { get; set; }
        public List<SelectList> ImageLists { get; set; }
        public string[] ProductImages { get; set; }
    }
}