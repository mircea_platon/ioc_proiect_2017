﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RegieMag.Startup))]
namespace RegieMag
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
